var connstring = 'amqp://snecwgnt:UaHDPOtXETkROLoIaPuWsiIcRZAw3EQt@emu.rmq.cloudamqp.com/snecwgnt';
var express = require('express');
var socket = require('socket.io');
var amqp = require('amqplib/callback_api');
var app = express();

var server = app.listen(4000, function(){
  console.log('listening on port 4000');
});

var io = socket(server);

io.on('connection', function(socket){
  amqp.connect(connstring, function(err, conn) {
    conn.createChannel(function(err, ch) {
      var ex = socket.handshake.query.subscription; // get subscription for exchange

      ch.assertExchange(ex, 'direct', {durable: true});

      ch.assertQueue('', {exclusive: true}, function(err, q) {
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
        ch.bindQueue(q.queue, ex, '');

        ch.consume(q.queue, function(msg) {
          socket.emit('recived', msg.content.toString());          
        }, {noAck: true});
      });
    });
  });
});

